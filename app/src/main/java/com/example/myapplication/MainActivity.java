package com.example.myapplication;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentActivity;

import com.llyun.common.dialog.DialogInput;

public class MainActivity extends FragmentActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DialogInput()
                        .setTitle("123")
                        .show(getSupportFragmentManager());
            }
        });

    }


}
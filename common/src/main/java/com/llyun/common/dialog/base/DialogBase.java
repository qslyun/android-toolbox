package com.llyun.common.dialog.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.llyun.common.R;

public abstract class DialogBase<T extends DialogBase> extends DialogFragment implements IDialog<T> {

    private DialogConfirmCallback<T> mConfirmCallback;
    private String mTitle;
    private boolean isShowCancel = true, isShowConfirm = true, isAutoDismissByConfirm = true;
    protected TextView tvTitle, tvCancel, tvConfirm;
    private View layoutBottom, vBottomCenterLine;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.dialog_normal);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), null);
        findViewByIdBase(view);
        initDataBase();
        initEventBase();
        findViewById(view);
        initData();
        return view;
    }

    private void findViewByIdBase(View view) {
        tvTitle = view.findViewById(R.id.tvTitle);
        tvCancel = view.findViewById(R.id.tvCancel);
        tvConfirm = view.findViewById(R.id.tvConfirm);
        layoutBottom = view.findViewById(R.id.layoutBottom);
        vBottomCenterLine = view.findViewById(R.id.vBottomCenterLine);
    }

    private void initDataBase() {
        if (mTitle != null) {
            tvTitle.setText(mTitle);
        }
        tvCancel.setVisibility(isShowCancel ? View.VISIBLE : View.GONE);
        tvConfirm.setVisibility(isShowConfirm ? View.VISIBLE : View.GONE);
        layoutBottom.setVisibility(isShowCancel || isShowConfirm ? View.VISIBLE : View.GONE);
        vBottomCenterLine.setVisibility(isShowCancel && isShowConfirm ? View.VISIBLE : View.GONE);
    }

    private void initEventBase() {
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mConfirmCallback != null) {
                    mConfirmCallback.confirm(getChildDialog());
                }
                if (isAutoDismissByConfirm) {
                    getDialog().dismiss();
                }
            }
        });
    }

    public T setConfirmCallback(DialogConfirmCallback<T> callback) {
        this.mConfirmCallback = callback;
        return getChildDialog();
    }

    public T setTitle(String title) {
        mTitle = title;
        return getChildDialog();
    }

    public T setShowCancal(boolean isShowCancel) {
        this.isShowCancel = isShowCancel;
        return getChildDialog();
    }

    public T setShowConfirm(boolean isShowConfirm) {
        this.isShowConfirm = isShowConfirm;
        return getChildDialog();
    }

    public T setAutoDismissByConfirm(boolean isAutoDismissByConfirm) {
        this.isAutoDismissByConfirm = isAutoDismissByConfirm;
        return getChildDialog();
    }

    public void show(@NonNull FragmentManager manager) {
        show(manager, "");
    }
}

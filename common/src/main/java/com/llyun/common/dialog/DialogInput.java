package com.llyun.common.dialog;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.llyun.common.R;
import com.llyun.common.dialog.base.DialogBase;

public class DialogInput extends DialogBase<DialogInput> {

    private EditText etContent;

    private String mHint;

    @Override
    public DialogInput getChildDialog() {
        return this;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_input;
    }

    @Override
    public void findViewById(View view) {
        etContent = view.findViewById(R.id.etContent);
    }

    @Override
    public void initData() {
        if (mHint != null) {
            etContent.setHint(mHint);
        }
    }

    @Override
    public void initEvent() {

    }

    public String getInputText() {
        return etContent.getText().toString();
    }

    public DialogInput setHint(String hint) {
        mHint = hint;
        return this;
    }
}

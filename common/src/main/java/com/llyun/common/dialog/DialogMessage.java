package com.llyun.common.dialog;

import android.view.View;
import android.widget.TextView;

import com.llyun.common.R;
import com.llyun.common.dialog.base.DialogBase;

public class DialogMessage extends DialogBase<DialogMessage> {

    private TextView tvContent;
    private String mContent;

    @Override
    public DialogMessage getChildDialog() {
        return this;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_message;
    }

    @Override
    public void findViewById(View view) {
        tvContent = view.findViewById(R.id.tvContent);
    }

    @Override
    public void initData() {
        if (mContent != null) {
            tvContent.setText(mContent);
        }
    }

    @Override
    public void initEvent() {

    }

    public DialogMessage setContent(String content) {
        mContent = content;
        return this;
    }
}

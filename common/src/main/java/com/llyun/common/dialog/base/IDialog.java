package com.llyun.common.dialog.base;

import android.view.View;

public interface IDialog<T extends DialogBase> {
    T getChildDialog();

    int getLayoutId();

    void findViewById(View view);

    void initData();

    void initEvent();
}

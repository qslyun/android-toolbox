package com.llyun.common.dialog.base;

public interface DialogConfirmCallback<T extends DialogBase> {
    void confirm(T t);
}
